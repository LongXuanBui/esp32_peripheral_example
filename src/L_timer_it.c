#include"L_timer_it.h"
void (*timer_gr0_call_back)(int );
void (*timer_gr1_call_back)(int );

static void inline print_timer_counter(uint64_t counter_value)
{
    printf("Counter: 0x%08x%08x\n", (uint32_t) (counter_value >> 32),
           (uint32_t) (counter_value));
    printf("Time   : %.8f s\n", (double) counter_value / TIMER_SCALE);
}
void IRAM_ATTR timer_group0_isr(void *para)
{
      timer_spinlock_take(TIMER_GROUP_0);
    int timer_idx = (int) para;
uint32_t timer_intr = timer_group_get_intr_status_in_isr(TIMER_GROUP_0);
    if (timer_intr & TIMER_INTR_T0) {

        timer_group_clr_intr_status_in_isr(TIMER_GROUP_0, TIMER_0);
    } else if (timer_intr & TIMER_INTR_T1) {
        timer_group_clr_intr_status_in_isr(TIMER_GROUP_0, TIMER_1);
} 
timer_gr0_call_back(timer_idx);
    timer_group_enable_alarm_in_isr(TIMER_GROUP_0, timer_idx);
        timer_spinlock_give(TIMER_GROUP_0);
}
void IRAM_ATTR timer_group1_isr(void *para)
{
      timer_spinlock_take(TIMER_GROUP_1);
    int timer_idx = (int) para;
    uint32_t timer_intr = timer_group_get_intr_status_in_isr(TIMER_GROUP_1);
    if (timer_intr & TIMER_INTR_T0) {
        timer_group_clr_intr_status_in_isr(TIMER_GROUP_1, TIMER_0);
    } else if (timer_intr & TIMER_INTR_T1) {
        timer_group_clr_intr_status_in_isr(TIMER_GROUP_1, TIMER_1);
    } 
timer_gr1_call_back(timer_idx);
timer_group_enable_alarm_in_isr(TIMER_GROUP_1, timer_idx);
        timer_spinlock_give(TIMER_GROUP_1);
}
void timer_set_callback_on_timer_gr0(void * fn)
{
timer_gr0_call_back=fn;
}
void timer_set_callback_on_timer_gr1(void * fn)
{
timer_gr1_call_back=fn;
}
void timer_it_init(timer_group_t timer_group,timer_idx_t timer_idx,bool auto_reload,double timer_interval_sec )
{
       /* Select and initialize basic parameters of the timer */
    timer_config_t config = {
        .divider = TIMER_DIVIDER,
        .counter_dir = TIMER_COUNT_UP,
        .counter_en = TIMER_PAUSE,
        .alarm_en = TIMER_ALARM_EN,
        .auto_reload = auto_reload,
    }; // default clock source is APB
        timer_init(timer_group, timer_idx, &config);
           /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(timer_group, timer_idx, 0x00000000ULL); 
    timer_set_alarm_value(timer_group, timer_idx, timer_interval_sec * TIMER_SCALE);
    timer_enable_intr(timer_group, timer_idx);
    if(timer_group==TIMER_GROUP_0)
    {
    timer_isr_register(timer_group, timer_idx, timer_group0_isr,
                       (void *) timer_idx, ESP_INTR_FLAG_IRAM, NULL);
    }else if (timer_group==TIMER_GROUP_1)
    {
          timer_isr_register(timer_group, timer_idx, timer_group1_isr,
                       (void *) timer_idx, ESP_INTR_FLAG_IRAM, NULL);
    }
    

    timer_start(timer_group, timer_idx);
}
