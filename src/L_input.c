#include "L_input.h"
void (*input_call_back)(int);
static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    uint32_t gpio_num = (uint32_t) arg;
    input_call_back(gpio_num);
}
 void ip_set_callback(void* fn)
{
   input_call_back=fn;
}
void ip_init(gpio_num_t gpio_num, gpio_int_type_t intr_type)
{
    // uint64_t pinbitmask=1<<(uint64_t)gpio_num;
    // gpio_config_t io_conf;
    //     //interrupt of rising edge
    // io_conf.intr_type = GPIO_PIN_INTR_NEGEDGE;
    // //bit mask of the pins, use GPIO "pin" here
    // io_conf.pin_bit_mask = pinbitmask;
    // //set as input mode    
    // io_conf.mode = GPIO_MODE_INPUT;
    // //enable pull-up mode
    // io_conf.pull_up_en = 1;
    // //configure GPIO with the given settings
    // gpio_config(&io_conf);
        //change gpio intrrupt type for one pin
    gpio_pad_select_gpio(gpio_num);
    gpio_set_direction(gpio_num,GPIO_MODE_INPUT );
    gpio_set_pull_mode(gpio_num,GPIO_PULLUP_ONLY);
    gpio_set_intr_type(gpio_num, intr_type);
    //install gpio isr service
    gpio_install_isr_service(0);
    //hook isr handler for specific gpio pin again
    gpio_isr_handler_add(gpio_num, gpio_isr_handler, (void*) gpio_num);
}