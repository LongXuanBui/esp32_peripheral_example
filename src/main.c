
#include "L_adc.h"
#include "L_timer_it.h"
#include "L_output.h"
#include "L_pwm.h"
#include "L_uart.h"
#define INTERVAL_TIME (100e-6)
#define SIZE 100
xQueueHandle timer_queue;
QueueHandle_t uart2;
char c[]="hello world\n";
uint8_t a[100];
int state=0;
void callback_timer_g0(int timer)
{
    if(timer==TIMER_1)
    {

    timer_event_t evt;
    evt.timer_group=TIMER_GROUP_0;
    evt.timer_idx=TIMER_1;
    xQueueSendFromISR(timer_queue,&evt,NULL);
    }
}
void timer_event_task(void *arg)
{
    timer_event_t rec;

    for ( ;;)
    {
        xQueueReceive(timer_queue,&rec,portMAX_DELAY );
        if(rec.timer_group==TIMER_GROUP_0&& rec.timer_idx==TIMER_1)
        {
    uint32_t val= adc_adc1_get_value(ADC1_CHANNEL_6);
    uint32_t vol= adc_get_voltage(val);
    printf("%d\t%d\n",val,vol);
    pwm_set_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0,state);
        }


    // vTaskDelay(1000/portTICK_PERIOD_MS);


    }
    
}
void uart_event_task(void *pvParameters)
{
uart_event_t event;
for ( ;;)
{
 if(xQueueReceive(uart2, (void * )&event, (portTickType)portMAX_DELAY))
{
     uart_read_bytes(2,a,event.size, portMAX_DELAY);
     if(a[0]=='a')
     {
    state++;
     op_set_level(23,state%2);
    uart_write_bytes(2,a,2);
     }
}
}
}

void app_main(void)
{
    op_init_gpio(23,GPIO_MODE_OUTPUT);
    timer_it_init(TIMER_GROUP_0,TIMER_1,TEST_WITH_RELOAD,INTERVAL_TIME);
    timer_set_callback_on_timer_gr0(callback_timer_g0);
    timer_queue = xQueueCreate(10, sizeof(timer_event_t));
    adc_check_efuse();
    adc_config(ADC_UNIT_1,ADC_CHANNEL_6);
    adc_characterize_adc(ADC_UNIT_1);
pwm_config_timer(LEDC_TIMER_0,LEDC_HIGH_SPEED_MODE,F_5kHz);
pwm_config_chanel(LEDC_TIMER_0,5,LEDC_CHANNEL_0,LEDC_HIGH_SPEED_MODE);
uart_set_queue(&uart2);
uart_init( 2, 115200, 17, 16);
    xTaskCreate(timer_event_task,"timer_event_task",2048,NULL,4,NULL);
    xTaskCreate(uart_event_task, "uart_event_task", 2048, NULL, 12, NULL);
}
