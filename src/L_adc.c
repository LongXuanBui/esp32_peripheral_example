#include "L_adc.h"
#define DEFAULT_VREF      1100      //Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES   64          //Multisampling
static esp_adc_cal_characteristics_t *adc_chars;
const adc_atten_t atten = ADC_ATTEN_DB_11;
const adc_bits_width_t width = ADC_WIDTH_BIT_12;
 void adc_check_efuse(void)
{

    //Check if TP is burned into eFuse
    if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {
        printf("eFuse Two Point: Supported\n");
    } else {
        printf("eFuse Two Point: NOT supported\n");
    }
    //Check Vref is burned into eFuse
    if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF) == ESP_OK) {
        printf("eFuse Vref: Supported\n");
    } else {
        printf("eFuse Vref: NOT supported\n");
    }

}
static void adc_print_char_val_type(esp_adc_cal_value_t val_type)
{
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP) {
        printf("Characterized using Two Point Value\n");
    } else if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
        printf("Characterized using eFuse Vref\n");
    } else {
        printf("Characterized using Default Vref\n");
    }
}
void adc_config(adc_unit_t unit,adc_channel_t channel)
{
      if (unit == ADC_UNIT_1) {
        adc1_config_width(width);
        adc1_config_channel_atten(channel, atten);
    } else {
        adc2_config_channel_atten((adc2_channel_t)channel, atten);
    }
}
void adc_characterize_adc(adc_unit_t unit)
{
    adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(unit, atten, width, DEFAULT_VREF, adc_chars);
    adc_print_char_val_type(val_type);
}
uint32_t  adc_adc1_get_value(adc1_channel_t channel)
{   uint32_t adc_reading = 0;
     for (int i = 0; i < NO_OF_SAMPLES; i++) {
                adc_reading += adc1_get_raw((adc1_channel_t)channel);
        }
        adc_reading /= NO_OF_SAMPLES;
return adc_reading;
}
uint32_t  adc_adc2_get_value(adc2_channel_t channel)
{
      uint32_t adc_reading = 0;
        for (int i = 0; i < NO_OF_SAMPLES; i++) {
                int raw;
                adc2_get_raw((adc2_channel_t)channel, width, &raw);
                adc_reading += raw;
        }
        adc_reading /= NO_OF_SAMPLES;
        return adc_reading;
}
uint32_t adc_get_voltage(uint32_t raw )
{
     uint32_t voltage = esp_adc_cal_raw_to_voltage(raw, adc_chars);
     return voltage;
}
