#include "L_pwm.h"
void pwm_config_timer(ledc_timer_t  timer_num,ledc_mode_t speed_mode,freq_t freq_hz)
{
      ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_13_BIT, // resolution of PWM duty
        .freq_hz = freq_hz,                      // frequency of PWM signal
        .speed_mode = speed_mode,           // timer mode
        .timer_num = timer_num,            // timer index
        .clk_cfg = LEDC_AUTO_CLK,              // Auto select the source clock
    };
ledc_timer_config(&ledc_timer);
}
void pwm_config_chanel( ledc_timer_t timer,int gpio_num,ledc_channel_t channel,ledc_mode_t speed_mode)
{
    ledc_channel_config_t ledc_channel = 
        {
            .channel    = channel,
            .duty       = 0,
            .gpio_num   = gpio_num,
            .speed_mode = speed_mode,
            .hpoint     = 0,
            .timer_sel  = timer,
        };
         ledc_channel_config(&ledc_channel);
}
void pwm_set_duty(ledc_mode_t speed_mode, ledc_channel_t channel, uint32_t duty)
{
            ledc_set_duty(speed_mode, channel, duty);
            ledc_update_duty(speed_mode, channel);
}