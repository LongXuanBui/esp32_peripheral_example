#include "L_uart.h"
#define BUF_SIZE (1024)
QueueHandle_t *queue;
const char *TAG = "uart_events";
void uart_set_queue( QueueHandle_t *uart_queue)
{
queue=uart_queue;
}
void uart_init(int uart_num, int baud_rate, int tx_io_num, int rx_io_num)
{
        esp_log_level_set(TAG, ESP_LOG_INFO);

        /* Configure parameters of an UART driver,
     * communication pins and install the driver */
    uart_config_t uart_config = {
        .baud_rate = baud_rate,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };
    //Install UART driver, and get the queue.
    uart_driver_install(uart_num, BUF_SIZE * 2, BUF_SIZE * 2, 20, queue, 0);
    uart_param_config(uart_num, &uart_config);

    //Set UART pins (using UART0 default pins ie no changes.)
    uart_set_pin(uart_num, tx_io_num, rx_io_num, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
}

