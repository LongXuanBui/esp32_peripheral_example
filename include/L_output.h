#pragma once
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
esp_err_t op_init_gpio(gpio_num_t gpio_num, gpio_mode_t mode);
esp_err_t op_set_level(gpio_num_t gpio_num,uint32_t level);