#pragma once

#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "esp_log.h"
void uart_init(int uart_num, int baud_rate, int tx_io_num, int rx_io_num);
void uart_set_queue( QueueHandle_t *uart_queue);
#ifdef __cplusplus
}
#endif