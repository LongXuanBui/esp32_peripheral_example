#pragma once
#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"
typedef enum{
F_1kHz=1000,
F_5kHz=5000,
F_10kHz=5000,
}freq_t;
void pwm_config_timer(ledc_timer_t  timer_num,ledc_mode_t speed_mode,freq_t freq_hz);
void pwm_config_chanel(ledc_timer_t timer,int gpio_num,ledc_channel_t channel,ledc_mode_t speed_mode);
void pwm_set_duty(ledc_mode_t speed_mode, ledc_channel_t channel, uint32_t duty);
#ifdef __cplusplus
}
#endif