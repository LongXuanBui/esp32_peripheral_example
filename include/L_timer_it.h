#pragma once
#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include "esp_types.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#define TIMER_DIVIDER         16  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER)  // convert counter value to seconds
#define TEST_WITHOUT_RELOAD   0        // testing will be done without auto reload
#define TEST_WITH_RELOAD      1        // testing will be done with auto reload
typedef struct {
    int type;  // the type of timer's event
    int timer_group;
    int timer_idx;
    uint64_t timer_counter_value;
} timer_event_t;
void timer_it_init(timer_group_t timer_group,timer_idx_t timer_idx,bool auto_reload,double timer_interval_sec );
void timer_set_callback_on_timer_gr0();
void timer_set_callback_on_timer_gr1();

#ifdef __cplusplus
}
#endif