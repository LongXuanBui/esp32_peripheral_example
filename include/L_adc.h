#pragma once
#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"

void adc_config(adc_unit_t unit,adc_channel_t channel);
void adc_check_efuse(void);
void adc_characterize_adc(adc_unit_t unit);
uint32_t  adc_adc1_get_value(adc1_channel_t channel);
uint32_t  adc_adc2_get_value(adc2_channel_t channel);
uint32_t adc_get_voltage(uint32_t raw);

#ifdef __cplusplus
}
#endif
